package org.ysb33r.sgcrdb.hibernate

import grails.orm.bootstrap.HibernateDatastoreSpringInitializer
import groovy.sql.Sql
import groovy.transform.CompileStatic
import org.hibernate.SessionFactory
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.orm.hibernate4.HibernateTransactionManager
import org.ysb33r.sgcrdb.ItemProcessor

import javax.sql.DataSource
import java.sql.Connection
import java.sql.SQLException
import java.sql.SQLFeatureNotSupportedException
import java.util.logging.Logger

/**
 * @author Schalk W. Cronjé
 */
class HibernateProcessor implements ItemProcessor {

    /** Constructs a processor that will write to a Hibernate backend
     *
     * @param configuration Hibernate and dataSource configuration. All dataSource properties should be prefixed
     * by {@code dataSource}. For specific properties see
     * {@link http://docs.groovy-lang.org/latest/html/api/groovy/sql/Sql.html#newInstance%28java.util.Map%29}
     */
    HibernateProcessor(Map configuration = [:]) {
        Map dataSource = configuration.dataSource
        String driverOptions = dataSource.findAll { k,v -> !(k =~ /driver|url|user|password/) }.
            collect { k,v -> "${k}=${v}" }.
            join(';')

        def dmDataSource = new DriverManagerDataSource(
//            configuration.'dataSource.driver' as String,
            "${dataSource.url};${driverOptions}".toString(),
            dataSource.user ?: '',
            dataSource.password ?: ''
        )
        final Map options = configuration.remove('dataSource')

        initializer = new HibernateDatastoreSpringInitializer(options,Item)
        applicationContext = initializer.configureForDataSource(dmDataSource)
//        transactionManager = new HibernateTransactionManager()
//        println "**** ${initializer.defaultSessionFactoryBeanName}"
    }

    /** Processes an item from the Saabs Grintek CRDB and writes it to a Hibernate-enabled
     * database
     *
     * @param item Map containing key value pairs of item details.
     */
    @Override
    void process(Map<String, String> item) {
        new Item(item).save()
    }

    private def initializer
    private def applicationContext
    private def transactionManager
}
