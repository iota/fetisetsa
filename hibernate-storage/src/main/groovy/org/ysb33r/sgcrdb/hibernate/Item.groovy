package org.ysb33r.sgcrdb.hibernate

import grails.gorm.annotation.Entity

/**
 * @author Schalk W. Cronjé
 */
@Entity
class Item {
    String batch
    String IDNumber
    String MSISDN
    String RNORoute
    String Action
    boolean processed = false
}
