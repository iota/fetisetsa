package org.ysb33r.sgcrdb.hibernate

import spock.lang.Specification


/**
 * @author Schalk W. Cronjé
 */
class HibernateProcessorSpec extends Specification {

    static final Map exampleItem = [
        batch : '20120927000000CRDB',
        IDNumber : '20120903134451VODASP278360864731078999',
        MSISDN   : '27836086499',
        RNORoute : 'D082',
        Action   : 'Port'
    ]

    def "Construct an Item object from a Map"() {
        given:
        Item item = new Item(exampleItem)

        expect:
        item.Action == 'Port'

    }

    def "Process an item from a map "() {
        given:

        def processor = new HibernateProcessor(
            'hibernate.log_sql'   : 'true',
            dataSource : [
                url      : 'jdbc:h2:mem:',
                driver   : 'org.h2.Driver',
                MVCC     : 'TRUE',
                LOCK_TIMEOUT : '10000',
                DB_CLOSE_ON_EXIT : 'FALSE'
            ]
        )

        when:
        processor.process(exampleItem)

        then:
        false
    }
}