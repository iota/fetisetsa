/*
 * Copyright (C) 2015 Schalk W. Cronjé
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For open-source software which would like to link to this library, but which is
 * licensed under a possible non-compatible OSS license an exception is granted,
 * that such software can be linked to this library under the version 3 of the
 * GNU Lesser General Public License instead.
 *
 * A commercial license is also offered for this software in
 * cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
 * distribute commercial modified binaries of this software without subjecting
 * that software to the GPL and its requirement to distribute source code.
 */
package digital.iota.fetisetsa.sgcrdb

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.transform.TupleConstructor
import groovy.util.logging.Slf4j
import groovy.util.slurpersupport.GPathResult
import digital.iota.fetisetsa.sgcrdb.impl.Ftp4JClient

import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLStreamReader
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.sax.SAXResult
import javax.xml.transform.stax.StAXSource

import java.util.regex.Pattern
import java.util.zip.GZIPInputStream

/** A client to connect to a Saabs-Grintek CRDB site to retrieve updated caller routing data.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
@TupleConstructor
@Slf4j
class CrdbClient {

    /** Location of downloadable files on server
     *
     */
    static final String PATH = 'DWNLDS'

    /** Pattern that will be used to identify downloadable files
     *
     */
    static final Pattern FILE_MATCHER = ~/\p{Alpha}{4,6}Download\p{Digit}{14}\.xml\.gz/

    /** The fields from CRDB that is of interest to us.
     *
     */
    static final List<String> CRDB_FIELDS = ['IDNumber', 'MSISDN', 'RNORoute', 'Action']

    /** The userid to access the remote server
     *
     */
    String userid

    /** The remote server FQDN
     *
     */
    String server

    /** The port to use on the remove server. Defaults to 10021
     *
     */
    int port = 10021

    /** FTPS Client to use
     *
     */
    FtpsClient ftpsClient

    /** Sets and obfuscates the password, so that it can be used withtin a Vfs URI
     *
     * @param pass Any character sequence
     */
     void setPassword(CharSequence pass) {
        this.password = pass.toString().toCharArray()
         //  "{${cryptor.encrypt(pass.toString())}}".toCharArray()
    }


    /** Setup a connection object to the remote server.
     *  The connection will not take place until a parse method is called
     */
    void connect() {
        assert userid != null
        assert password != null
        assert server != null


        if(ftpsClient == null) {
            ftpsClient = new Ftp4JClient()
        }
        ftpsClient.connect(server, userid, password, port)
    }

    /** Disconnects a current session.
     *
     */
    void disconnect() {
        try {
            ftpsClient.disconnect()
        } catch (Exception e) {
            // DO nothing
        }
    }

    /** Allows for object to be configured and managed within a closure, thereby providing a lightweight DSL.
     *
     * @param cfg Delegating closure
     * @return Returns whtever the result of the last action in the closure was
     */
    def call(@DelegatesTo(CrdbClient) Closure cfg) {
        Closure runner = cfg.clone() as Closure
        runner.delegate = this
        runner()
    }

    /** Parses each item in a local Saabs-Grintek CRDB XML file, delegating each item to a closure
     * which can process it.
     *
     * @param xmlFile Processes the specified local file.
     * @param action Closure which takes a single {@code Map<String,String>} as input
     */
    void parseLocalFile(final File xmlFile,Closure action) {
        try {
            log.info "Parsing ${xmlFile}"
            xmlFile.withInputStream { InputStream strm ->
                parseXml(strm) { final String batch, final GPathResult activatedNumber ->
                    parse(batch, activatedNumber, action as ItemProcessor)
                }
            }
        } catch (final Exception e) {
            throw new CrdbException("Could not parse input file '${xmlFile}'",e)
        }
    }

    /** Traverses an XmlSlurper and delegate each item to a provided processor.
     *
     * @param fileXml XmlSlurper that has been populated
     * @param action Anything that implements the @link ItemProcessor interface.
     */

    /** Prses a subtree result from an XML file.
     *
     * @param batch Batch this subseciton forms part of
     * @param activatedNumber A XmlSlurper result for an {@code ActivatedNumber} node
     * @param action Action to perform on the parsed data.
     */
    @CompileDynamic
    void parse(final String batch,final GPathResult activatedNumber,ItemProcessor action) {
        boolean goodToGo = true
        Map<String,String> map = [ batch : batch ]
        CRDB_FIELDS.each { final String key ->
            def result = activatedNumber."${key}".text()

            if((result == null || result.empty) && key == 'MSISDN') {
                String from = activatedNumber.DNRanges.DNFrom.text()
                String to = activatedNumber.DNRanges.DNTo.text()

                if(from !=null && to !=null && !from.empty && !to.empty) {
                    map['DNRanges'] = result = [ from : from, to : to ]
                } else {
                    log.error "Missing ${key} / DNRanges field for item ${activatedNumber} in batch ${batch}"
                    goodToGo = false
                }
            } else if (result == null || result.empty) {
                log.error "Missing ${key} field for item ${activatedNumber} in batch ${batch}"
                goodToGo = false
            } else {
                map[key] = result
            }
        }
        log.debug "Processing ${map}"
        if(goodToGo) {
            action.process(map)
        }
    }

    /** Retrieves a Saabs-Grintek CRDB XML file from the remote end.
     *
     * @param uri If not null retrieves the given remote filename (not uri), otherwise find the first file and retrieve it
     * @param action Action to perform on the parsed data. Closure is passed a map of parsed entities from XML.
     * @throw CrdbException if no file can be processed
     */
    void parseRemoteFile(final String fileName=null,Closure action) {
        parseRemoteFile(fileName,action as ItemProcessor)
    }

    /** Retrieves a Saabs-Grintek CRDB XML file from the remote end.
     *
     * @param uri If not null retrieves the given remote filename (not uri), otherwise find the first file and retrieve it
     * @param action Action to perform on the parsed data
     * @throw CrdbException if no file can be processed
     */
    void parseRemoteFile(final String fileName=null,ItemProcessor action) {
        String getThisFile = fileName
        if(getThisFile == null) {
            List<String> names = remoteFileNames
            if(names.empty) {
                throw new CrdbException("There are no remote files to load")
            }
            getThisFile = names[0]
        }

        try {
            final File output = File.createTempFile('sgcrdbxml','$$$')
            output.deleteOnExit()
            log.info "Decompressing remote file ${getThisFile}"
            ftpsClient.catFile getThisFile, { InputStream strm ->
                final GZIPInputStream gunzip = new GZIPInputStream(strm)

                try {
                    output.withOutputStream { writer ->
                        writer << gunzip
                    }
                } finally {
                    gunzip.close()
                }
            }

            log.info "Parsing decompressed vesion of ${getThisFile}"

            output.withInputStream { reader ->
                parseXml(reader) { final String batch, final GPathResult activatedNumber ->
                    parse(batch,activatedNumber,action)
                }
            }

        } catch (final Exception e) {
            throw new CrdbException("Cannot process remote file ${fileName}",e)
        }
    }

    /** Parses all files on the remote site.
     *
     * @param action Object implementing the {@link ItemProcessor} interface.
     * @throw CrdbException if no file can be processed
     */
    void parseAllFiles(ItemProcessor action) {
        log.info "Parsing all files"

        ftpsClient.listFiles PATH, FILE_MATCHER, { String name ->
            log.info "Parsing ${name}"
            ftpsClient.catFile "/${PATH}/${name}", { InputStream strm ->
                parseXml(new GZIPInputStream(strm)) { final String batch, final GPathResult activatedNumber ->
                    parse(batch,activatedNumber,action)
                }
            }
        }
    }

    /** Parses all files on the remote site.
     *
     * @param action Closure which takes a single {@code Map<String,String>} as input
     * @throw CrdbException if no file can be processed
     */
    void parseAllFiles(Closure action) {
        parseAllFiles(action as ItemProcessor)
    }


    /** Returns a list of filenames
     *
     * @return List of remote filenames
     * @throw CrdbException upon failure
     */
    List<String> getRemoteFileNames() {
        try {
            List<String> names = []
            ftpsClient.listFiles(PATH,FILE_MATCHER) { String it ->
                names+= it
            }
            return names.sort()
        } catch(final Exception e) {
            throw new CrdbException("Could not list remote files",e)
        }
    }


    /** Access to the password is only accessible from within the same package.
     *
     * @return THe obfuscated password
     */
    @PackageScope
    char[] getPassword() {
        this.password
    }

    private def parseXml(final InputStream strm,Closure parseActivatedNumber) {
        String batch
        XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(strm)
        Transformer transformer = TransformerFactory.newInstance().newTransformer()

        XmlSlurper xmlSlurper = new XmlSlurper()
        def saxResult = new SAXResult(xmlSlurper)
        def source = new StAXSource(xmlStreamReader)

        while(xmlStreamReader.hasNext()) {
            xmlStreamReader.next()
            if (xmlStreamReader.isStartElement() && xmlStreamReader.getLocalName() == 'IDNumber' && batch == null) {
                transformer.transform(source, saxResult)
                batch = xmlSlurper.document.text()
            } else if (xmlStreamReader.isStartElement() && xmlStreamReader.getLocalName() == 'ActivatedNumber') {
                transformer.transform(source, saxResult)
                parseActivatedNumber(batch,xmlSlurper.document)
            }
        }
    }

    private char[] password
}
