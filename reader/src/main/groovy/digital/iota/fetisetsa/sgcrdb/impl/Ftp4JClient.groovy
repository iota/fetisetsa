/*
 * Copyright (C) 2015 Schalk W. Cronjé
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For open-source software which would like to link to this library, but which is
 * licensed under a possible non-compatible OSS license an exception is granted,
 * that such software can be linked to this library under the version 3 of the
 * GNU Lesser General Public License instead.
 *
 * A commercial license is also offered for this software in
 * cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
 * distribute commercial modified binaries of this software without subjecting
 * that software to the GPL and its requirement to distribute source code.
 */
package digital.iota.fetisetsa.sgcrdb.impl

import groovy.transform.CompileStatic
import it.sauronsoftware.ftp4j.FTPClient
import it.sauronsoftware.ftp4j.FTPDataTransferListener
import digital.iota.fetisetsa.sgcrdb.FtpsClient

import java.util.regex.Pattern

/** A warapper around ftp4j.
 *
 * @since 0.10.0
 */
@CompileStatic
class Ftp4JClient implements FtpsClient {

    Ftp4JClient(boolean tls = true) {
        ftpClient = new FTPClient()
        ftpClient.setType(FTPClient.TYPE_BINARY)
        ftpClient.setPassive(true)
        if(tls) {
            ftpClient.setSecurity(FTPClient.SECURITY_FTPES)
        }
    }

    /** Connects to the remote server with the given credentials
     *
     * @param server
     * @param userid
     * @param pass
     * @param port
     */
    @Override
    void connect(String server, String userid, char[] pass, int port) {
        ftpClient.connect(server,port)
        ftpClient.login(userid,pass.toString())
    }

    /** Disconnect from existing sesion.
     *
     * If already disconnected, do nothing.
     */
    @Override
    void disconnect() {
        ftpClient.disconnect(false)
    }

    /** Cat the given file
     *
     * @param fileName Name of file
     * @param cfg Closure to process the content. The closure will be passed an {@code InputStream} as parameter.
     */
    @Override
    void catFile(String fileName, Closure cfg) {
        File supplied = new File(fileName)
        File path = supplied.parentFile
        if(path!= null && !path.name.empty) {
            ftpClient.changeDirectory(path.toString())
        }

        File output = File.createTempFile('ftp4j','$$$')
        ftpClient.download(supplied.name,output)
        output.withInputStream cfg
        output.deleteOnExit()
    }

    /** List files in path that matches a pattern
     *
     * @param path Path on remote server to list
     * @param filter FIlter to apply to file names
     * @param action Action to perform on each found file. The name of each file is passed to the closure.
     */
    @Override
    void listFiles(String path, Pattern filter, Closure action) {
        ftpClient.changeDirectory(path)
        ftpClient.listNames().each { String name ->
            if( name =~ filter) {
                action(name)
            }
        }
    }

    private FTPClient ftpClient

    private static class TransferListener implements FTPDataTransferListener {

        void started() {
        }

        void transferred(int length) {
        }

        void completed() {
        }

        void aborted() {
        }

        void failed() {
        }

    }
}
