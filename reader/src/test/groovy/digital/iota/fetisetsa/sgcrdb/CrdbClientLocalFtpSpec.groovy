/*
 * Copyright (C) 2015 Schalk W. Cronjé
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For open-source software which would like to link to this library, but which is
 * licensed under a possible non-compatible OSS license an exception is granted,
 * that such software can be linked to this library under the version 3 of the
 * GNU Lesser General Public License instead.
 *
 * A commercial license is also offered for this software in
 * cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
 * distribute commercial modified binaries of this software without subjecting
 * that software to the GPL and its requirement to distribute source code.
 */
package digital.iota.fetisetsa.sgcrdb
//import org.apache.commons.vfs2.util.Cryptor
//import org.apache.commons.vfs2.util.CryptorFactory
import digital.iota.fetisetsa.sgcrdb.impl.Ftp4JClient
import digital.iota.fetisetsa.sgcrdb.internal.FtpServer
//import spock.lang.Ignore
//import spock.lang.Shared
import spock.lang.Specification

/**
 * @author Schalk W. Cronjé
 */
class CrdbClientLocalFtpSpec extends Specification {

    final String TESTFILENAME = 'DCRDBDownload20120928020037.xml.gz'
    final File TESTFILEDIR = new File( System.getProperty('TESTFILEDIR') ?: './src/test/resources' )
    final FtpServer server = new FtpServer( TESTFILEDIR )

    def crdb = new CrdbClient( server : 'localhost',userid:'guest', password:'guest', port : FtpServer.PORT )

    void setup() {
        server.start()
        crdb.ftpsClient = new Ftp4JClient(false)
    }

    void cleanup() {
        server.stop()
    }

    def "Connect and download xml file"() {

        given:
        crdb.connect()

        when:
        List files = crdb.remoteFileNames

        then:
        files.size() == 1
        files[0] == TESTFILENAME

        when:
        List xml = []
        crdb.parseRemoteFile(files[0]) {

            xml+= it
        }

        then:
        xml.size()

        xml[0].batch == '20120927000000CRDB'
        xml[0].IDNumber == '20120924102632CELLC076851656702399'

    }

    def "Download and parse all appropriate files"() {
        given:
        List entries = []
        crdb.connect()
        crdb.parseAllFiles  { Map m ->
            entries+= m
        }

        expect:
        entries.size() == 7
        entries[0] == [
            batch : '20120927000000CRDB',
            IDNumber : '20120924102632CELLC076851656702399',
            MSISDN : '277685165699',
            RNORoute : 'D082',
            Action : 'Reverse']
        entries[0] != entries [1]
        entries[6] == [
            batch : '20120927000000CRDB',
            IDNumber : '20120903134451VODASP278360864731078999',
            MSISDN   : '27836086499',
            RNORoute : 'D082',
            Action   : 'Port'
        ]

    }
}