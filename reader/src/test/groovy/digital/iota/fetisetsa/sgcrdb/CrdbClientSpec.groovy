/*
 * Copyright (C) 2015 Schalk W. Cronjé
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For open-source software which would like to link to this library, but which is
 * licensed under a possible non-compatible OSS license an exception is granted,
 * that such software can be linked to this library under the version 3 of the
 * GNU Lesser General Public License instead.
 *
 * A commercial license is also offered for this software in
 * cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
 * distribute commercial modified binaries of this software without subjecting
 * that software to the GPL and its requirement to distribute source code.
 */
package digital.iota.fetisetsa.sgcrdb

import digital.iota.fetisetsa.sgcrdb.impl.Ftp4JClient
import spock.lang.Shared
import spock.lang.Specification


/**
 * @author Schalk W. Cronjé
 */
class CrdbClientSpec extends Specification {

    final File TESTFILEDIR = new File( System.getProperty('TESTFILEDIR') ?: './src/test/resources' )

    def crdb = new CrdbClient()
    @Shared def backupSystemErr = System.err

    void setup() {
        crdb.ftpsClient = new Ftp4JClient(false)
    }

    void cleanup() {
        System.err = backupSystemErr
    }

    def "Create client and configure"() {
        given:
//        final Cryptor cryptor = CryptorFactory.getCryptor()
        crdb {
            server = 'example.local'
            userid = 'user'
            password = 'pass'
        }

        expect:
        crdb.server == 'example.local'
        crdb.userid == 'user'
        crdb.password.toString() =='pass'
    }


    def "Parse a good local XML file"() {
        given:
        List entries = []
        crdb.parseLocalFile new File(TESTFILEDIR,'good.xml'), { Map m ->
            entries+= m
        }

        expect:
        entries.size() == 8
        entries[0] == [
            batch : '20120927000000CRDB',
            IDNumber : '20120924102632CELLC076851656702399',
            MSISDN : '277685165699',
            RNORoute : 'D082',
            Action : 'Reverse']
        entries[0] != entries [1]
        entries[6] == [
            batch : '20120927000000CRDB',
            IDNumber : '20120903134451VODASP278360864731078999',
            MSISDN   : '27836086499',
            RNORoute : 'D082',
            Action   : 'Port'
        ]
        entries[7] == [
            batch : '20120927000000CRDB',
            IDNumber : '20160120102512SKYCALL27110264450CRC401',
            RNORoute : 'D082',
            Action   : 'Port',
            DNRanges : [ from : '27110264450', to : '27110264455' ]
        ]
    }

    def "Parsing a file with incomplete items, will cause errors to be logged, but processing will continue"() {
        given:
        ByteArrayOutputStream systemErr = new ByteArrayOutputStream()
        System.err = new PrintStream(systemErr)
        List entries = []

        when:
        crdb.parseLocalFile new File(TESTFILEDIR, 'MissingFields.xml'), { Map m ->
            entries += m
        }
        System.err.flush()

        then:
        entries.size() == 1

    }

    def "Parse an incomplete local XML file results in an exception"() {
        given:
        List entries = []

        when:
        crdb.parseLocalFile new File(TESTFILEDIR, 'incomplete.xml'), { Map m ->
            entries += m
        }

        then:
        thrown(CrdbException)
    }

}