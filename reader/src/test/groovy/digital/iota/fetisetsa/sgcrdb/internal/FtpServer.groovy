/*
 * Copyright (C) 2015 Schalk W. Cronjé
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For open-source software which would like to link to this library, but which is
 * licensed under a possible non-compatible OSS license an exception is granted,
 * that such software can be linked to this library under the version 3 of the
 * GNU Lesser General Public License instead.
 *
 * A commercial license is also offered for this software in
 * cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
 * distribute commercial modified binaries of this software without subjecting
 * that software to the GPL and its requirement to distribute source code.
 */
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2015
//
// This has been reworked from the Groovy-VFS project and this file remains under
// the same license as below.
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================

// After an idea from http://snipplr.com/view/64953/

package digital.iota.fetisetsa.sgcrdb.internal

import org.apache.ftpserver.ConnectionConfigFactory
import org.apache.ftpserver.FtpServerFactory
import org.apache.ftpserver.ftplet.Authority
import org.apache.ftpserver.listener.ListenerFactory
import org.apache.ftpserver.ssl.SslConfigurationFactory
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory
import org.apache.ftpserver.usermanager.impl.BaseUser
import org.apache.ftpserver.usermanager.impl.WritePermission

class FtpServer {

    static final def PORT  = System.getProperty('FTPPORT') ?: 50021   

    def server

    FtpServer( File readRoot  ) {
        
        def userManagerFactory = new PropertiesUserManagerFactory()
        def userMgr = userManagerFactory.createUserManager()
        def serverFactory = new FtpServerFactory()
        def factory = new ListenerFactory()
        def configFactory = new ConnectionConfigFactory()

        def ssl = new SslConfigurationFactory()
        ssl.setKeystoreFile(new File("src/test/resources/clientkeystore.jks"))
        ssl.setKeystorePassword("password");

        def user = new BaseUser()
        ArrayList<Authority> auths = []
        def auth = new WritePermission()

        auths.add(auth)
        user.setName( 'guest' )
        user.setPassword( 'guest' )
        user.setHomeDirectory( readRoot.absolutePath )
        user.setAuthorities(auths)
        userMgr.save(user)

        configFactory.maxLogins = 20
        serverFactory.userManager = userMgr
        serverFactory.connectionConfig = configFactory.createConnectionConfig()
        factory.setPort( PORT )
//        factory.setSslConfiguration(ssl.createSslConfiguration())
//        factory.setImplicitSsl(true)
        serverFactory.addListener("default", factory.createListener())
        
        server = serverFactory.createServer();
    } 
    
    void start() {
        server.start()
    }
    
    void stop() {
        server.stop()
    }
    

}
