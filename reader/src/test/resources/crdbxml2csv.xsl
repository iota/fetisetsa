<!--

    Copyright (C) 2015 Schalk W. Cronjé

    This library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    For open-source software which would like to link to this library, but which is
    licensed under a possible non-compatible OSS license an exception is granted,
    that such software can be linked to this library under the version 3 of the
    GNU Lesser General Public License instead.

    A commercial license is also offered for this software in
    cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
    distribute commercial modified binaries of this software without subjecting
    that software to the GPL and its requirement to distribute source code.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output encoding="UTF-8" method="text"/>

    <xsl:template match="/">
        <xsl:text>batch,IDNumber,MSISDN,RNORoute,Action,processed&#10;</xsl:text>
        <xsl:apply-templates select="//ActivatedNumber"/>
    </xsl:template>

    <xsl:template match="ActivatedNumber">
        <xsl:variable name="batch" select="../../IDNumber/text()"/>
        <xsl:value-of select="concat($batch,',')"/>
        <xsl:value-of select="concat(IDNumber/text(),',')"/>
        <xsl:choose>
            <xsl:when test="DNRanges">
                <xsl:value-of select="concat(DNRanges/DNFrom/text(),'-',DNRanges/DNTo/text(),',')"/>
            </xsl:when>
            <xsl:when test="MSISDN">
                <xsl:value-of select="concat(MSISDN/text(),',')"/>
            </xsl:when>
            <xsl:otherwise><xsl:text>NO_NUMBER</xsl:text></xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="concat(RNORoute/text(),',0&#10;')"/>
    </xsl:template>

</xsl:stylesheet>

