/*
 * Copyright (C) 2015 Schalk W. Cronjé
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For open-source software which would like to link to this library, but which is
 * licensed under a possible non-compatible OSS license an exception is granted,
 * that such software can be linked to this library under the version 3 of the
 * GNU Lesser General Public License instead.
 *
 * A commercial license is also offered for this software in
 * cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
 * distribute commercial modified binaries of this software without subjecting
 * that software to the GPL and its requirement to distribute source code.
 */
package digital.iota.fetisetsa.sgcrdb.simplesql

import groovy.sql.Sql
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import digital.iota.fetisetsa.sgcrdb.ItemProcessor

/**
 * @author Schalk W. Cronjé
 */
@CompileStatic
class SimpleSqlProcessor implements ItemProcessor {

    static final String TABLE_NAME = 'SaabGrintekUpdates'
    static final String INSERT_CMD = "INSERT INTO ${TABLE_NAME} (batch,IDNumber,MSISDN,RNORoute,Action) VALUES( ?,?,?,?,? )"

    Sql sql

    SimpleSqlProcessor(Map dataSource) {
        newSqlInstance(dataSource)
        createTableIfNotExisting()
    }

/** Processes an item from the Saabs Grintek CRDB
     *
     * @param item Map containing key value pairs of item details.
     */
    @Override
    void process(Map<String, String> item) {
        sql.execute INSERT_CMD,[item.batch,item.IDNumber,item.MSISDN,item.RNORoute,item.Action] as List<Object>
    }

    @CompileDynamic
    private void newSqlInstance(Map dataSource) {
        String driverOptions = dataSource.findAll { k,v -> !(k =~ /driver|url/) }.
            collect { k,v -> "${k}=${v}" }.
            join(';')

        sql = Sql.newInstance("${dataSource.url};${driverOptions}",dataSource.driver)

    }

    private void createTableIfNotExisting() {
        try {
            final String query = "SELECT COUNT(*) FROM ${TABLE_NAME}"
            sql.execute query
        }
        catch(SQLException) {
            final String create = """CREATE TABLE ${TABLE_NAME} (
                batch varchar(20) not null,
                IDNumber varchar(60) not null,
                MSISDN varchar(20) not null,
                RNORoute varchar(6) not null,
                Action varchar(20) not null,
                processed int default 0
            )"""
            sql.execute create
        }
    }

}
