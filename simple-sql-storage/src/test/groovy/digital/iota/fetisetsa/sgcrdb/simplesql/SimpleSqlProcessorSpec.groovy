/*
 * Copyright (C) 2015 Schalk W. Cronjé
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For open-source software which would like to link to this library, but which is
 * licensed under a possible non-compatible OSS license an exception is granted,
 * that such software can be linked to this library under the version 3 of the
 * GNU Lesser General Public License instead.
 *
 * A commercial license is also offered for this software in
 * cases where GPL is not appropriate. This allows OEMs, ISVs and VARs to
 * distribute commercial modified binaries of this software without subjecting
 * that software to the GPL and its requirement to distribute source code.
 */
package digital.iota.fetisetsa.sgcrdb.simplesql

import spock.lang.Specification


/**
 * @author Schalk W. Cronjé
 */
class SimpleSqlProcessorSpec extends Specification {

    static final Map exampleItem = [
        batch : '20120927000000CRDB',
        IDNumber : '20120903134451VODASP278360864731078999',
        MSISDN   : '27836086499',
        RNORoute : 'D082',
        Action   : 'Port'
    ]

    def "Process an item from a map "() {
        given:

        def processor = new SimpleSqlProcessor(
                url      : 'jdbc:h2:mem:',
                driver   : 'org.h2.Driver',
                MVCC     : 'TRUE',
                LOCK_TIMEOUT : '10000'
        )

        when:
        processor.process(exampleItem)
        def query = processor.sql.rows("SELECT * FROM ${SimpleSqlProcessor.TABLE_NAME}".toString())

        then:
        query.size() == 1
        query[0].batch == exampleItem.batch
    }
}